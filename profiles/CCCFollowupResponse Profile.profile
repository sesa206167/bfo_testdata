<?xml version="1.0" encoding="UTF-8"?>
<Profile xmlns="http://soap.sforce.com/2006/04/metadata">
<applicationVisibilities><application>standard__Platform</application><default>true</default><visible>true</visible></applicationVisibilities>
<custom>true</custom>
<fieldPermissions><editable>true</editable><field>ACC_PartnerProgram__c.ProgramLevel__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Account.AccountSource</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Account.AnnualRevenue</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Account.BillingAddress</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Account.Description</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Account.Fax</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Account.Jigsaw</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Account.NumberOfEmployees</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Account.ParentId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Account.Phone</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Account.SVMXC__Access_Hours__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Account.SVMXC__Business_Hours__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Account.SVMXC__Preferred_Technician__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Account.ShippingAddress</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Account.SicDesc</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Account.Website</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>AccountChallenge__c.RelatedAccountPlan__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>AccountContactRelation.EndDate</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>AccountContactRelation.IsActive</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>AccountContactRelation.Roles</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>AccountContactRelation.StartDate</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>AccountEnergyUsage__c.Account__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>AccountUpdateRequest__c.Account__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Announcement__c.Offer_Lifecycle__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Asset.AccountId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Asset.ContactId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Asset.Description</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Asset.InstallDate</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Asset.IsCompetitorProduct</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Asset.Price</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Asset.Product2Id</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Asset.PurchaseDate</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Asset.Quantity</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Asset.SerialNumber</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Asset.Status</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Asset.StockKeepingUnit</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Asset.UsageEndDate</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>CSE_RelatedProduct__c.Case__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Campaign.ActualCost</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Campaign.AmountAllOpportunities</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Campaign.AmountWonOpportunities</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Campaign.BudgetedCost</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Campaign.CampaignImageId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Campaign.Description</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Campaign.EndDate</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Campaign.ExpectedResponse</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Campaign.ExpectedRevenue</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Campaign.IsActive</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Campaign.NumberOfContacts</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Campaign.NumberOfConvertedLeads</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Campaign.NumberOfLeads</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Campaign.NumberOfOpportunities</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Campaign.NumberOfResponses</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Campaign.NumberOfWonOpportunities</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Campaign.NumberSent</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Campaign.ParentId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Campaign.StartDate</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Campaign.Status</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Campaign.Type</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Case.AccountId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Case.ClosedDate</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.ContactId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.Description</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Case.IsEscalated</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.Origin</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.ParentId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.Priority</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.Reason</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Actual_Initial_Response__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Actual_Onsite_Response__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Actual_Resolution__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Actual_Restoration__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Auto_Entitlement_Status__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__BW_Date__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__BW_Selected_By__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__BW_Selected_On__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__BW_Slots_Before__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__BW_Territory__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__BW_Time_Zone__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Billing_Type__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Booking_Window__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Clock_Paused_Forever__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Component_City__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Component_Country__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Component_State__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Component_Street__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Component_Zip__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Component__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Entitlement_Notes__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Entitlement_Type__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Initial_Response_Customer_By__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Initial_Response_Internal_By__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Is_PM_Case__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Is_SLA_Calculated__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Onsite_Response_Customer_By__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Onsite_Response_Internal_By__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__PM_Plan__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Perform_Auto_Entitlement__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Preferred_End_Time__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Preferred_Start_Time__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Product__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Resolution_Customer_By__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Resolution_Internal_By__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Restoration_Customer_By__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Restoration_Internal_By__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__SContract_Business_Hour__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__SLA_Clock_Extension_Minutes__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__SLA_Clock_Pause_Days__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__SLA_Clock_Pause_Hours__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__SLA_Clock_Pause_Minutes__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__SLA_Clock_Pause_Reason__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__SLA_Clock_Pause_Restart_Time__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__SLA_Clock_Pause_Time__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__SLA_Clock_Paused__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__SLA_Terms__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Scheduled_Date__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Service_Contract__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Site__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Top_Level__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.SVMXC__Warranty__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.Subject</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Case.SuppliedCompany</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Case.SuppliedEmail</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Case.SuppliedName</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Case.SuppliedPhone</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Case.Type</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>CommercialReference__c.CommercialReference__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.AccountId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.AssistantName</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.AssistantPhone</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.Birthdate</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.Department</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.Description</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.Email</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.Fax</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.HomePhone</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.Jigsaw</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.LeadSource</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.MailingAddress</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.MobilePhone</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.OtherAddress</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.OtherPhone</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.Phone</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.ReportsToId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contact.Title</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>ContactAssignedFeature__c.PartnerProgram__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>ContactAssignedProgram__c.ProgramLevel__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Contract.ActivatedById</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Contract.ActivatedDate</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contract.BillingAddress</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contract.CompanySignedDate</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contract.CompanySignedId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contract.ContractTerm</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contract.CustomerSignedDate</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contract.CustomerSignedId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contract.CustomerSignedTitle</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contract.Description</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>Contract.EndDate</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contract.OwnerExpirationNotice</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contract.ShippingAddress</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contract.SpecialTerms</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Contract.StartDate</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>CustomerCareTeam__c.CCCountry__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>EmailToCaseKeyword__c.CCCountry__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Event.Description</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Event.IsAllDayEvent</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Event.Location</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Event.WhatId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Event.WhoId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>ExternalPropertiesCatalog__c.FunctionalKey__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>ExternalPropertiesCatalog__c.SubType__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>FieloPRM_MemberFeature__c.PRMUIMSId__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>IndustryMembership__c.IndustryMembership__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Lead.Address</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Lead.AnnualRevenue</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Lead.Description</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Lead.Email</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Lead.Industry</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Lead.Jigsaw</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Lead.LeadSource</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Lead.NumberOfEmployees</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Lead.Phone</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Lead.Rating</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Lead.SFGA__CorrelationID__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Lead.SFGA__Correlation_Data__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Lead.SFGA__Web_Source__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Lead.Title</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Lead.Website</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LegacyAccount__c.Account__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveAgentSession.ChatReqAssigned</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveAgentSession.ChatReqDeclined</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveAgentSession.ChatReqEngaged</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveAgentSession.ChatReqTimedOut</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveAgentSession.TimeAtCapacity</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveAgentSession.TimeIdle</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveAgentSession.TimeInAwayStatus</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveAgentSession.TimeInChats</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveAgentSession.TimeInOnlineStatus</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.AccountId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.AverageResponseTimeOperator</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.AverageResponseTimeVisitor</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.Body</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.Browser</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.BrowserLanguage</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.CaseId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.ChatKey</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.ContactId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.EndTime</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.EndedBy</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.IpAddress</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.LeadId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.LiveChatButtonId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.LiveChatDeploymentId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.Location</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.OperatorMessageCount</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.Platform</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.ReferrerUri</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.RequestTime</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.ScreenResolution</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.SkillId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.StartTime</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.Status</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.SupervisorTranscriptBody</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.UserAgent</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LiveChatTranscript.VisitorMessageCount</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>LocalizedFSB__c.Organization__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>MemberExternalProperties__c.ExternalPropertiesCatalog__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Milestone1_Task__c.Project_Milestone__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Milestone1_Task__c.Task_Stage__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>OPP_SupportRequest__c.Opportunity__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Opportunity.AccountId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Opportunity.Amount</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Opportunity.CampaignId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Opportunity.Description</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Opportunity.LeadSource</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Opportunity.NextStep</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Opportunity.Probability</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Opportunity.Type</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>OpportunityLineItem.Description</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>OpportunityLineItem.ListPrice</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>OpportunityLineItem.ProductCode</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>OpportunityLineItem.ServiceDate</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>false</editable><field>OpportunityLineItem.TotalPrice</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>OpportunityRegistrationForm__c.AccountName__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>OpportunityRegistrationForm__c.Amount__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>OpportunityRegistrationForm__c.ExpectedOrderDate__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PRMBadgeBFOProperties__c.FieldName__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PRMBadgeBFOProperties__c.PRMCountryFieldName__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PRMBadgeBFOProperties__c.TableName__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PRMBadgeBFOProperties__c.Value__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PartnerFundAllocation.Description</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PartnerFundClaim.Description</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PartnerFundClaim.Status</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PartnerFundRequest.Activity</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PartnerFundRequest.AllocationId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PartnerFundRequest.Amount</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PartnerFundRequest.BudgetId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PartnerFundRequest.CampaignId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PartnerFundRequest.Description</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PartnerFundRequest.DesiredOutcome</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PartnerFundRequest.Status</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PartnerMarketingBudget.Description</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>PartnerOpportunityStatusHistory__c.Opportunity__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Problem__c.Title__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Problem__c.WhereWasTheProblemDetected__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.Description</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.DisplayUrl</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.ExternalDataSourceId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.ExternalId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.Family</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.ProductCode</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.QuantityUnitOfMeasure</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.SVMXC__Enable_Serialized_Tracking__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.SVMXC__Inherit_Parent_Warranty__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.SVMXC__Product_Cost__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.SVMXC__Product_Line__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.SVMXC__Replacement_Available__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.SVMXC__Select__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.SVMXC__Stockable__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.SVMXC__Tracking__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.SVMXC__Unit_Of_Measure__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Product2.StockKeepingUnit</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>ProfessionalMembership__c.ProfessionalMembership__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>ProgramLevelBrand__c.ProgramLevel__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>ProgramRequirement__c.PartnerProgram__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>RMA__c.Case__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>RangesPerDeviceAndBrand__c.Range__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>SFE_EstimatedPlannedSales__c.AccountPlan__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>SOSSession.AppVersion</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>SOSSession.CaseId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>SOSSession.ContactId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>SOSSession.DeploymentId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>SOSSession.EndTime</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>SOSSession.IpAddress</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>SOSSession.OpentokSession</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>SOSSession.SessionDuration</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>SOSSession.SessionRecordingUrl</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>SOSSession.SessionToken</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>SOSSession.SosVersion</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>SOSSession.StartTime</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>SOSSession.SystemInfo</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>SOSSession.WaitDuration</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Solution.IsPublished</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Solution.IsPublishedInPublicKb</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Solution.SolutionNote</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Task.ActivityDate</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Task.Description</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Task.WhatId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Task.WhoId</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>TeamProductJunction__c.ProductFamily__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>Technical_Return__c.DCReceivingDate__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>User.SVMXC__Dispatch_Console_Settings__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>User.SVMXC__FaceTime_ID__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>User.SVMXC__Is_Super_Dispatcher__c</field><readable>true</readable></fieldPermissions>
<fieldPermissions><editable>true</editable><field>User.SVMXC__Select__c</field><readable>true</readable></fieldPermissions>
<layoutAssignments><layout>ACC_Channel__c-CNL - Channel Layout</layout></layoutAssignments>
<layoutAssignments><layout>ACC_LocalAttribute__c-LAL - Local Attribute Layout</layout></layoutAssignments>
<layoutAssignments><layout>ACC_PartnerProgram__c-Account Partner Program Layout</layout></layoutAssignments>
<layoutAssignments><layout>ANI__c-ANI Layout</layout></layoutAssignments>
<layoutAssignments><layout>APApexTestClass__c-Apex Test Class Layout</layout></layoutAssignments>
<layoutAssignments><layout>ARApprovalQueueMapping__mdt-AR Approval Queue Mapping Layout</layout></layoutAssignments>
<layoutAssignments><layout>AS_ActiveUsers__c-AS-Active User Layout</layout></layoutAssignments>
<layoutAssignments><layout>AS_LastLoginsFilter__c-AS-Last Login w%2F connexion type filter Layout</layout></layoutAssignments>
<layoutAssignments><layout>AS_LastLoginsNoFilter__c-AS-Last Login w%2Fo connexion type filter Layout</layout></layoutAssignments>
<layoutAssignments><layout>Account-ACC - Standard Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountAssessment__c-Account Assessment Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountAssignedFeature__c-AAF - Account Partner Program Feature Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountAssignedRequirement__c-ARQ - Activity Requirement Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountBrand-Account BrandLayout</layout></layoutAssignments>
<layoutAssignments><layout>AccountChallenge__c-Account Challenge Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountContactRelation-Account Contact Relationship Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountElectricianInfo__c-Electrician-specific Info Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountEnergyUsage__c-Account Energy Usage Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountGrowthInitiative__c-Account Growth Initiative Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountGrowthPlan__c-Account Growth Plan Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountMasterProfilePAM__c-Account Master Profile PAM Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountMasterProfile__c-Account Master Profile Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountOwnerChangeTracking__c-Account Owner Change Tracking Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountOwnershipRule__c-Account Ownership Rule Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountPlanConfidential__c-Account Plan Confidential Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountSpecializationRequirement__c-Account Specialization Requirement Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountSpecialization__c-Account Specialization Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountTeamMember-Account Team Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>AccountUpdateRequest__c-Country AUR Layout</layout></layoutAssignments>
<layoutAssignments><layout>AcknowledgmentTemplate__c-Acknowledgment Template Layout</layout></layoutAssignments>
<layoutAssignments><layout>Action__c-Action Layout</layout></layoutAssignments>
<layoutAssignments><layout>AlertTranslations__c-User System Alert Layout</layout></layoutAssignments>
<layoutAssignments><layout>Analysis_Conclusion__c-Analysis Conclusion Layout</layout></layoutAssignments>
<layoutAssignments><layout>AnnouncementStakeholder__c-Announcement Stakeholder Layout</layout></layoutAssignments>
<layoutAssignments><layout>Announcement_Groups__c-Announcement Group Layout</layout></layoutAssignments>
<layoutAssignments><layout>Announcement__c-Announcement Layout</layout></layoutAssignments>
<layoutAssignments><layout>Answertostack__c-Answer-to stack Layout</layout></layoutAssignments>
<layoutAssignments><layout>ApexDebugLog__c-Apex Debug Log Layout</layout></layoutAssignments>
<layoutAssignments><layout>App__c-App Layout</layout></layoutAssignments>
<layoutAssignments><layout>ArchiveCSE_ExternalReferences__c-Archive External Reference Layout</layout></layoutAssignments>
<layoutAssignments><layout>ArchiveCaseComments__c-Archvie Case Comments Layout</layout></layoutAssignments>
<layoutAssignments><layout>ArchiveCaseStage__c-Archive Case Stage Layout</layout></layoutAssignments>
<layoutAssignments><layout>ArchiveCase__c-Archive Case Layout</layout></layoutAssignments>
<layoutAssignments><layout>ArchiveEmailMessage__c-ArchiveEmailMessageLayout</layout></layoutAssignments>
<layoutAssignments><layout>ArchiveInquiraFAQ__c-Archive Inquira FAQ Layout</layout></layoutAssignments>
<layoutAssignments><layout>ArchiveInstalledProduct__c-Archive Installed Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>ArchiveLog__c-Case Archive Log Layout</layout></layoutAssignments>
<layoutAssignments><layout>AssessementProgram__c-Assessement Program Layout</layout></layoutAssignments>
<layoutAssignments><layout>AssessmentCountry__c-Assessment Country Layout</layout></layoutAssignments>
<layoutAssignments><layout>Assessment__c-Assessment Layout</layout></layoutAssignments>
<layoutAssignments><layout>Asset-Asset Layout</layout></layoutAssignments>
<layoutAssignments><layout>AssetLocationEvent__c-Asset %2F Location Event Layout</layout></layoutAssignments>
<layoutAssignments><layout>AssetRelationship-Asset Relationship Layout</layout></layoutAssignments>
<layoutAssignments><layout>AssignedToolsTechnicians__c-Assigned Tools and Technicians Layout</layout></layoutAssignments>
<layoutAssignments><layout>AssociateWorkOrderNotification__c-Associate Work Order Notification Layout</layout></layoutAssignments>
<layoutAssignments><layout>AssociatedInstalledProduct__c-Associated Installed Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>Average_selling_price__c-Average selling price Layout</layout></layoutAssignments>
<layoutAssignments><layout>BFOEAMulticurrency__c-BFOEAMulticurrency Layout</layout></layoutAssignments>
<layoutAssignments><layout>BackOfficesystem__c-Back Office system Layout</layout></layoutAssignments>
<layoutAssignments><layout>Badge_Certificate__c-Badge Certificate Layout</layout></layoutAssignments>
<layoutAssignments><layout>BatchImport__c-Batch Import Info Layout</layout></layoutAssignments>
<layoutAssignments><layout>BeneficiaryPackage__c-BPK - General Layout</layout></layoutAssignments>
<layoutAssignments><layout>BfoWSEndPoint__mdt-Org Type Layout</layout></layoutAssignments>
<layoutAssignments><layout>Brand__c-Brand Layout</layout></layoutAssignments>
<layoutAssignments><layout>Budget__c-Budget Layout</layout></layoutAssignments>
<layoutAssignments><layout>BudgetaryOfferForm__c-Budgetary Offer Form Layout</layout></layoutAssignments>
<layoutAssignments><layout>BusinessRiskEscalationArchiveCaseLink__c-Business Risk - ACase link Layout</layout></layoutAssignments>
<layoutAssignments><layout>BusinessRiskEscalationCaseLink__c-BRCL - Business Risk - Case link Layout</layout></layoutAssignments>
<layoutAssignments><layout>BusinessRiskEscalationEntity__c-BREE - Business Risk Entity Layout</layout></layoutAssignments>
<layoutAssignments><layout>BusinessRiskEscalationStakeholder__c-BRS - Business Risk Stakeholder Layout</layout></layoutAssignments>
<layoutAssignments><layout>BusinessRiskEscalations__c-BRE - Business Risk Layout</layout></layoutAssignments>
<layoutAssignments><layout>BusinessSpecificInformation__c-Buildings - Energy Solutions Layout</layout></layoutAssignments>
<layoutAssignments><layout>Business_Requirement__c-Business Requirement Layout</layout></layoutAssignments>
<layoutAssignments><layout>Business_Risk_Escalation_Alert__c-Business Risk Escalation Alert Layout</layout></layoutAssignments>
<layoutAssignments><layout>CAPCountryWeight__c-CAP Country Weight Layout</layout></layoutAssignments>
<layoutAssignments><layout>CAPGlobalMasterData__c-CAP Global Master Data Layout</layout></layoutAssignments>
<layoutAssignments><layout>CBATeam__c-Contributing Business Approver Layout</layout></layoutAssignments>
<layoutAssignments><layout>CCCAction__c-CCA - CCC Action Layout</layout></layoutAssignments>
<layoutAssignments><layout>CCCLocation__c-CCC Location Layout</layout></layoutAssignments>
<layoutAssignments><layout>CCC_ChatButton__c-CCC ChatButton Layout</layout></layoutAssignments>
<layoutAssignments><layout>CHR_ChangeReq__c-Change Request Layout</layout></layoutAssignments>
<layoutAssignments><layout>CIS_Competency__c-CIS Competency Layout</layout></layoutAssignments>
<layoutAssignments><layout>CIS_CorrespondentsTeam__c-CIS Correspondents Team Layout</layout></layoutAssignments>
<layoutAssignments><layout>CIS_CountryListPrice__c-Country Local Cost Layout</layout></layoutAssignments>
<layoutAssignments><layout>CIS_FieldServiceRepresentative__c-Field Service Representative Layout</layout></layoutAssignments>
<layoutAssignments><layout>CIS_ISRComment__c-ISR Comment Layout</layout></layoutAssignments>
<layoutAssignments><layout>CIS_ISR__c-ISR Layout</layout></layoutAssignments>
<layoutAssignments><layout>CIS_InterventionCosts__c-Intervention costs Layout</layout></layoutAssignments>
<layoutAssignments><layout>CIS_ServiceCenter__c-Service Center Layout</layout></layoutAssignments>
<layoutAssignments><layout>CMCCCMySE__mdt-CM CCC mySE Layout</layout></layoutAssignments>
<layoutAssignments><layout>CON_LocalAttribute__c-Contact Local Attribute Layout</layout></layoutAssignments>
<layoutAssignments><layout>CR_Comments__c-C%5CR Comments Layout</layout></layoutAssignments>
<layoutAssignments><layout>CSE_ExternalReferences__c-ERF - External References Layout</layout></layoutAssignments>
<layoutAssignments><layout>CSE_L2L3InternalComments__c-LIC - L2L3 Internal Comments Layout</layout></layoutAssignments>
<layoutAssignments><layout>CSE_RelatedProduct__c-Case Related Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>CSQ_CertificationStatus__c-CSQ Certification status Layout</layout></layoutAssignments>
<layoutAssignments><layout>CSQ_CertificationType__c-CSQ Certification type Layout</layout></layoutAssignments>
<layoutAssignments><layout>CSQ_CounterMeasure__c-Counter Measure Layout</layout></layoutAssignments>
<layoutAssignments><layout>CSQ_ImprovementProject__c-Core Model Layout</layout></layoutAssignments>
<layoutAssignments><layout>CSQ_Measure__c-Measure Layout</layout></layoutAssignments>
<layoutAssignments><layout>CSQ_Profile__c-CSQ Profile Layout</layout></layoutAssignments>
<layoutAssignments><layout>CSQ_ProjectTeamMember__c-Project Team Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>CTR_ValueChainPlayers__c-Contract Value Chain Player Layout</layout></layoutAssignments>
<layoutAssignments><layout>CWTMember__c-CWT Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>Campaign-Campaign Layout</layout></layoutAssignments>
<layoutAssignments><layout>CampaignCountry__c-Campaign Country Layout</layout></layoutAssignments>
<layoutAssignments><layout>CampaignMember-Campaign Member Page Layout</layout></layoutAssignments>
<layoutAssignments><layout>Capability__c-Capability Layout</layout></layoutAssignments>
<layoutAssignments><layout>CareAgent__c-Care Agent Layout</layout></layoutAssignments>
<layoutAssignments><layout>Case-CSE - General Layout</layout></layoutAssignments>
<layoutAssignments><layout>CaseClassification__c-CCL - Layout</layout></layoutAssignments>
<layoutAssignments><layout>CaseClose-CSE - Close Case Layout</layout></layoutAssignments>
<layoutAssignments><layout>CaseRoutingRule__c-Case Routing Rule Layout</layout></layoutAssignments>
<layoutAssignments><layout>CaseStage__c-Case Stage Layout</layout></layoutAssignments>
<layoutAssignments><layout>CaseStakeholders__c-Case Stakeholders Layout</layout></layoutAssignments>
<layoutAssignments><layout>Category__c-Category Layout</layout></layoutAssignments>
<layoutAssignments><layout>CertificationCatalog__c-Certification Layout</layout></layoutAssignments>
<layoutAssignments><layout>ChangeRequestCostsForecast__c-Change Request Costs Forecasts Layout</layout></layoutAssignments>
<layoutAssignments><layout>ChangeRequestFundingEntity__c-Change Request Funding Entity Layout</layout></layoutAssignments>
<layoutAssignments><layout>ChangeRequestStakeholder__c-Change Request Stakeholder Layout</layout></layoutAssignments>
<layoutAssignments><layout>Change_Request_Link__c-Change Request Link Layout</layout></layoutAssignments>
<layoutAssignments><layout>Change_Request__c-Change Request CREATE Status</layout></layoutAssignments>
<layoutAssignments><layout>ChannelProgram-Channel Program Layout</layout></layoutAssignments>
<layoutAssignments><layout>ChannelProgramLevel-Channel Program Level Layout</layout></layoutAssignments>
<layoutAssignments><layout>ChannelProgramMember-Channel Program Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>ChannelSystemAlert__c-Channel System Alert Layout</layout></layoutAssignments>
<layoutAssignments><layout>ClassificationLevelCatalog__c-Classification Level Catalog Layout</layout></layoutAssignments>
<layoutAssignments><layout>CoachingVisit__c-Coaching Visit Layout</layout></layoutAssignments>
<layoutAssignments><layout>CollaborationGroup-Group Layout</layout></layoutAssignments>
<layoutAssignments><layout>CollaborationWorkingTeam__c-Collaboration Working Team Layout</layout></layoutAssignments>
<layoutAssignments><layout>CommercialReference__c-CMR - Commercial Reference Layout</layout></layoutAssignments>
<layoutAssignments><layout>CommunityMemberLayout-Community Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>CompetitorCountry__c-CCO - Competitor Country Layout</layout></layoutAssignments>
<layoutAssignments><layout>Competitor__c-CMP - Competitor Layout</layout></layoutAssignments>
<layoutAssignments><layout>ComplaintRequestRoutingRule__c-Complaint Request Routing Rule Layout</layout></layoutAssignments>
<layoutAssignments><layout>ComplaintRequest__c-Complaint Request External</layout></layoutAssignments>
<layoutAssignments><layout>ComplaintsRequestsEscalationHistory__c-Complaints Requests Escalation History Layout</layout></layoutAssignments>
<layoutAssignments><layout>ComponentParameter__c-Component Parameters Segements Layout</layout></layoutAssignments>
<layoutAssignments><layout>ComponentProfileMap__c-Component Profile Map Layout</layout></layoutAssignments>
<layoutAssignments><layout>Component__c-Component Layout</layout></layoutAssignments>
<layoutAssignments><layout>ConfigureORFField__mdt-Configure ORF Field Layout</layout></layoutAssignments>
<layoutAssignments><layout>ConnectedSystem__c-Connected System Layout</layout></layoutAssignments>
<layoutAssignments><layout>Connector_Contact__c-Connector Contact Layout</layout></layoutAssignments>
<layoutAssignments><layout>Connector__c-Connector Layout</layout></layoutAssignments>
<layoutAssignments><layout>Contact-CTC - Standard Layout</layout></layoutAssignments>
<layoutAssignments><layout>ContactAssignedFeature__c-CAF - Standard Layout</layout></layoutAssignments>
<layoutAssignments><layout>ContactAssignedProgram__c-CAP - Standard Layout</layout></layoutAssignments>
<layoutAssignments><layout>ContactAssignedRequirement__c-CAR - Activity Requirement Layout</layout></layoutAssignments>
<layoutAssignments><layout>ContactElectricianSpecificInfo__c-Contact Electrician Specific Info Layout</layout></layoutAssignments>
<layoutAssignments><layout>ContactEmailHistory__c-Contact Email History Layout</layout></layoutAssignments>
<layoutAssignments><layout>ContainmentAction__c-CNA - Internal Containment Action Layout</layout></layoutAssignments>
<layoutAssignments><layout>ContentAsset-Asset File Layout</layout></layoutAssignments>
<layoutAssignments><layout>ContentVersion-General</layout></layoutAssignments>
<layoutAssignments><layout>Contract-Contract Layout</layout></layoutAssignments>
<layoutAssignments><layout>ContractCustomerLocation__c-Contract Customer Location Layout</layout></layoutAssignments>
<layoutAssignments><layout>ContractExternalReference__c-Contract External Reference Layout</layout></layoutAssignments>
<layoutAssignments><layout>ContractProductInformation__c-CPI - General Layout</layout></layoutAssignments>
<layoutAssignments><layout>Convert_Attachments_to_Files_Log__c-Convert Attachments to Files Log Layout</layout></layoutAssignments>
<layoutAssignments><layout>Convert_Notes_to_ContentNotes_Log__c-Convert Notes to Enhanced Notes Log Layout</layout></layoutAssignments>
<layoutAssignments><layout>CorrectiveAction__c-CRA - Schneider Corrective Action Layout</layout></layoutAssignments>
<layoutAssignments><layout>CountryCISCorrespondentsTeamLink__c-Country CIS Correspondents Team Link Layout</layout></layoutAssignments>
<layoutAssignments><layout>CountryCategoryMapping__c-Country Category Mapping Layout</layout></layoutAssignments>
<layoutAssignments><layout>CountryChannelUserRegistration__c-Country Channel User Registration Fields Layout</layout></layoutAssignments>
<layoutAssignments><layout>CountryChannels__c-Country Channels Layout</layout></layoutAssignments>
<layoutAssignments><layout>CountryMapping__mdt-CountryMapping Layout</layout></layoutAssignments>
<layoutAssignments><layout>CountryStateLanguageTranslation__c-Country%2FState Language Translation Layout</layout></layoutAssignments>
<layoutAssignments><layout>Country__c-CTR - Layout</layout></layoutAssignments>
<layoutAssignments><layout>CoveredOrganization__c-Covered BU%2FGeography Layout</layout></layoutAssignments>
<layoutAssignments><layout>Covered_Country__c-Covered Country Layout</layout></layoutAssignments>
<layoutAssignments><layout>CreditRequestPriceList__c-Credit Request Price List Layout</layout></layoutAssignments>
<layoutAssignments><layout>CreditRequest__c-Credit Request Layout</layout></layoutAssignments>
<layoutAssignments><layout>CustomCategoryTreeLevel__c-Custom Category Tree Level Layout</layout></layoutAssignments>
<layoutAssignments><layout>CustomCategoryTree__c-OCM Custom Tree Layout</layout></layoutAssignments>
<layoutAssignments><layout>CustomRecentlyViewed__c-Recently Viewed Layout</layout></layoutAssignments>
<layoutAssignments><layout>CustomerCareTeam__c-CCT - Page Layout</layout></layoutAssignments>
<layoutAssignments><layout>CustomerLocation__c-Customer Location Layout</layout></layoutAssignments>
<layoutAssignments><layout>CustomerQuickQuestion__c-Customer Quick Question Layout</layout></layoutAssignments>
<layoutAssignments><layout>CustomerSafetyIssue__c-Customer Satisfaction Issue Layout</layout></layoutAssignments>
<layoutAssignments><layout>CustomerSatisfactionSurvey__c-CustomerSatisfactionSurvey Layout</layout></layoutAssignments>
<layoutAssignments><layout>CustomerVisitKPI__c-Customer Visit KPI Layout</layout></layoutAssignments>
<layoutAssignments><layout>Customer_Satisfaction_Survey_Roles__c-Customer Satisfaction Survey Roles Layout</layout></layoutAssignments>
<layoutAssignments><layout>Customer_Voice__c-CNPS 2%2E0 Layout</layout></layoutAssignments>
<layoutAssignments><layout>DA_MergeAccountField__mdt-DA Merge Account Field Layout</layout></layoutAssignments>
<layoutAssignments><layout>DA_SelfScheduledBatch__mdt-Self-Scheduled Batch Info Layout</layout></layoutAssignments>
<layoutAssignments><layout>DBAdditionalInformation__c-D%26B Additional Information Layout</layout></layoutAssignments>
<layoutAssignments><layout>DMTAuthorizationMasterData__c-DMT Authorization Layout</layout></layoutAssignments>
<layoutAssignments><layout>DMTDomainOwnerEmail__c-DMT Domain Owner Email Layout</layout></layoutAssignments>
<layoutAssignments><layout>DMTFundingEntity__c-DMT Funding Entity Layout</layout></layoutAssignments>
<layoutAssignments><layout>DMTProgram__c-DMT Program Layout</layout></layoutAssignments>
<layoutAssignments><layout>DMTStakeholder__c-DMT Stakeholder Layout</layout></layoutAssignments>
<layoutAssignments><layout>DMT_Application__c-Application Layout</layout></layoutAssignments>
<layoutAssignments><layout>DOF_potential_opportunity__c-Potential Opportunity Layout</layout></layoutAssignments>
<layoutAssignments><layout>DaUserInfo__c-Delegated Admin User Information Layout</layout></layoutAssignments>
<layoutAssignments><layout>DataTemplate__c-Data Template Layout</layout></layoutAssignments>
<layoutAssignments><layout>Datapoint__c-Datapoint Layout</layout></layoutAssignments>
<layoutAssignments><layout>DebugLog__c-Debug Log Layout</layout></layoutAssignments>
<layoutAssignments><layout>Decision__c-Decision Layout</layout></layoutAssignments>
<layoutAssignments><layout>Defect_OLD_DO_NOT_USE__c-Defect Layout</layout></layoutAssignments>
<layoutAssignments><layout>Defect__c-Defect Layout</layout></layoutAssignments>
<layoutAssignments><layout>DeploymentPlaybook__c-Deployment Playbook Layout</layout></layoutAssignments>
<layoutAssignments><layout>Deployment__c-Deployment Layout</layout></layoutAssignments>
<layoutAssignments><layout>DeviceType__c-Device Type Layout</layout></layoutAssignments>
<layoutAssignments><layout>DeviceTypesPerBrand__c-Device Types Per Brand Layout</layout></layoutAssignments>
<layoutAssignments><layout>DigitalToolsUserInformation__c-MySE User Information Layout</layout></layoutAssignments>
<layoutAssignments><layout>DirectAccountApprovalQueueMapping__c-Direct Account Approval Queue Mapping Layout</layout></layoutAssignments>
<layoutAssignments><layout>Discount__c-Discount Layout</layout></layoutAssignments>
<layoutAssignments><layout>DiscussionTopic__c-Discussion Topic Layout</layout></layoutAssignments>
<layoutAssignments><layout>DistributorRetailerSKU__c-Distributor Retailer SKU Layout</layout></layoutAssignments>
<layoutAssignments><layout>DomainsOfExpertiseCatalog__c-DOE - Domains Of Expertise Catalog Layout</layout></layoutAssignments>
<layoutAssignments><layout>DuplicateRecordItem-Duplicate Record Item Layout</layout></layoutAssignments>
<layoutAssignments><layout>DuplicateRecordSet-Duplicate Record Set Layout</layout></layoutAssignments>
<layoutAssignments><layout>EA_Geography_Mapping__c-EA Geography Mapping Layout</layout></layoutAssignments>
<layoutAssignments><layout>EA_Picklist_Mapping__c-EA Picklist Mapping Layout</layout></layoutAssignments>
<layoutAssignments><layout>EA_RowLevelSecurity__c-EA RowLevelSecurity Layout</layout></layoutAssignments>
<layoutAssignments><layout>EUS_EndUserSupport__c-EUS - Layout</layout></layoutAssignments>
<layoutAssignments><layout>Eligible_Entity__c-Eligible Entity Layout</layout></layoutAssignments>
<layoutAssignments><layout>EmailMessage-Email Message Layout - Email-to-Case</layout></layoutAssignments>
<layoutAssignments><layout>EmailToCaseKeyword__c-KWD - Page Layout</layout></layoutAssignments>
<layoutAssignments><layout>EndUserSupportEscalationHistory__c-End User Support Escalation History Layout</layout></layoutAssignments>
<layoutAssignments><layout>EnergySupplySustainability__c-Energy Supply %26 Sustainability Layout</layout></layoutAssignments>
<layoutAssignments><layout>Entities__c-Entity Layout</layout></layoutAssignments>
<layoutAssignments><layout>EntityStakeholder__c-ESH - Entity Stakeholder Layout</layout></layoutAssignments>
<layoutAssignments><layout>Environment__c-Environment Layout</layout></layoutAssignments>
<layoutAssignments><layout>Event-EVT - Standard Layout</layout></layoutAssignments>
<layoutAssignments><layout>Events__c-Events Layout</layout></layoutAssignments>
<layoutAssignments><layout>Exception__c-Exception Layout</layout></layoutAssignments>
<layoutAssignments><layout>ExpertInternalComment__c-Expert Internal Comment Layout</layout></layoutAssignments>
<layoutAssignments><layout>ExternalPropertiesCatalogType__c-External Systems Type Layout</layout></layoutAssignments>
<layoutAssignments><layout>ExternalPropertiesCatalog__c-External Properties Catalog Layout</layout></layoutAssignments>
<layoutAssignments><layout>ExternalSECertification__c-External %26 SE Certification Layout</layout></layoutAssignments>
<layoutAssignments><layout>FPF_TechnicalReturn__c-FPF - Technical Return Layout</layout></layoutAssignments>
<layoutAssignments><layout>FSBAffectedProduct__c-FSB Affected Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>FSBTeam__c-FSB Team Layout</layout></layoutAssignments>
<layoutAssignments><layout>FSM_Rel_Cont_to_L__c-Related Contact to Location Layout</layout></layoutAssignments>
<layoutAssignments><layout>FSM_Rel_cont_to_CL__c-Related contact to Customer Location Layout</layout></layoutAssignments>
<layoutAssignments><layout>FailedCase__c-Failed Case Layout</layout></layoutAssignments>
<layoutAssignments><layout>FailedEmailMessage__c-Failed Email Message Layout</layout></layoutAssignments>
<layoutAssignments><layout>FeatureCatalog__c-FEC - Standard Layout</layout></layoutAssignments>
<layoutAssignments><layout>FeatureRequirement__c-FER - Standard Layout</layout></layoutAssignments>
<layoutAssignments><layout>FeedItem-Feed Item Layout</layout></layoutAssignments>
<layoutAssignments><layout>Feedback__c-Feedback Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieldServiceBulletin__c-Field Service Bulletin - Problem Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieldValue__c-Field Value Layout</layout></layoutAssignments>
<layoutAssignments><layout>Field__c-Checkbox Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloCH__ChallengeMember__c-Challenge Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloCH__ChallengeMissionMember__c-Challenge Mission Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloCH__ChallengeMission__c-Challenge Mission Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloCH__Challenge__c-Challenge Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloCH__TeamChallenge__c-Team Challenge Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloCH__TeamMember__c-Team Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloCH__Team__c-Team Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloCRUD__ListView__c-List View</layout></layoutAssignments>
<layoutAssignments><layout>FieloCRUD__ObjectSettings__c-Object Settings</layout></layoutAssignments>
<layoutAssignments><layout>FieloCRUD__PageComponent__c-Field</layout></layoutAssignments>
<layoutAssignments><layout>FieloCRUD__PageLayout__c-Page Layout Standard</layout></layoutAssignments>
<layoutAssignments><layout>FieloCRUD__PageSection__c-Page Section</layout></layoutAssignments>
<layoutAssignments><layout>FieloDRE__RuleApplied__c-Fielo Rule Applied Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__BadgeMember__c-BadgeMember Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__Banner__c-Banner</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__Category__c-Category Group</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__Component__c-Banner</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__ErrorLog__c-Custom Error Log Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__Event__c-Event Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__MemberSegment__c-Custom Member Segment Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__Member__c-Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__Menu__c-menu2</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__News__c-News Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__Program__c-Program Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__Promotion__c-Standard Promotion</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__RedemptionItem__c-Formato de Order Item</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__RedemptionRule__c-Dynamic</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__Redemption__c-Formato de Redemption</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__Reward__c-Reward Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__Rule__c-Standard Rule</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__Section__c-Fielo - Section - Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__Tag__c-Standard Tag</layout></layoutAssignments>
<layoutAssignments><layout>FieloEE__Transaction__c-Formato de Transaction</layout></layoutAssignments>
<layoutAssignments><layout>FieloPRM_BadgeAccount__c-Badge Account Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloPRM_BadgeFeature__c-Badge Feature Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloPRM_BadgeMemberHistory__c-Badge Member History Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloPRM_BatchManagementConfiguration__c-Batch Management Configuration Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloPRM_BatchManagementView__c-Batch Management View Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloPRM_EventCatalog__c-Event Catalog Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloPRM_Feature__c-Feature Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloPRM_InvoiceDetail__c-PRM Invoice Detail Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloPRM_Invoice__c-PRM Invoice Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloPRM_LoyaltyEligibleProduct__c-PRM Loyalty Eligible Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloPRM_MemberFeatureDetail__c-Level</layout></layoutAssignments>
<layoutAssignments><layout>FieloPRM_MemberFeature__c-Member Feature Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloPRM_MemberUpdateHistory__c-Member Update History Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloPRM_PartnerLocatorConfiguration__c-Partner Locator Configuration Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloPRM_ProductPointList__c-Point List Layout</layout></layoutAssignments>
<layoutAssignments><layout>FieloPRM_ProductPointValue__c-PRM Product Point Value Layout</layout></layoutAssignments>
<layoutAssignments><layout>Fielo_CRUDObjectSettings__c-CRUD Object Settings Layout</layout></layoutAssignments>
<layoutAssignments><layout>Fielo_InvoiceDetails__c-Fielo - Invoice Detail Layout</layout></layoutAssignments>
<layoutAssignments><layout>Fielo_Invoice__c-Fielo - Invoice Layout</layout></layoutAssignments>
<layoutAssignments><layout>Fielo_ListView__c-List View Layout</layout></layoutAssignments>
<layoutAssignments><layout>Fielo_LoyaltyEligibleProduct__c-Fielo - Loyalty Eligible Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>Fielo_PageComponent__c-Page Component Layout</layout></layoutAssignments>
<layoutAssignments><layout>Fielo_PageLayout__c-Page Layout Layout</layout></layoutAssignments>
<layoutAssignments><layout>Fielo_PageSection__c-Page Section Layout</layout></layoutAssignments>
<layoutAssignments><layout>Floorwalking__c-Support Request %2F Floorwalking Layout</layout></layoutAssignments>
<layoutAssignments><layout>FlowInterview-Flow Interview Layout</layout></layoutAssignments>
<layoutAssignments><layout>GCSSite__c-Administrator - Site Layout</layout></layoutAssignments>
<layoutAssignments><layout>Global-Global Layout</layout></layoutAssignments>
<layoutAssignments><layout>GroupAccountTeamMember__c-Group Account Team Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>GroupAccountTeam__c-Group Account Team Layout</layout></layoutAssignments>
<layoutAssignments><layout>HabilitationRequirement__c-Account Layout</layout></layoutAssignments>
<layoutAssignments><layout>Hotfix_With_Component__c-Hotfix With Component Layout</layout></layoutAssignments>
<layoutAssignments><layout>Hotfix__c-Hot Fix Approved Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMSApplicationsMapping__c-IDMSApplicationsMapping Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMSCompanyReconciliationBatchHandler__c-IDMSCompanyReconciliationBatchHandler Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMSErrorLog__mdt-IDMSErrorLog Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMSGenerateUser__c-IDMSGenerateUser Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMSPRMTrustMapping__mdt-IDMS PRM Trust Mapping Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMSReconciliationBatchLog__c-IDMSReconciliationBatchLog Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMSUIMSResponse__c-IDMSUIMSResponse Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMSUserAIL__c-IDMS User AIL Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMSUserCompanyReLinkBatchHandler__c-IDMSUserCompanyReLinkBatchHandler Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMSUserInvitation__c-IDMSUserInvitation Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMSUserMaping__mdt-IDMS User Mapping Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMSUserReconciliationBatchHandler__c-IDMSUserReconciliationBatchHandler Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMS_ApplicationBrandingMatrix__mdt-IDMS Application Baranding Matrix Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMS_ApplicationMapping__mdt-IDMSApplicationMapping Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMS_EmailMapping__mdt-IDMS_EmailMapping Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMS_Email_Log__c-IDMS Email Log Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMS_ErrorLog__c-IDMS_ErrorLog Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMS_GDPRImplementation__mdt-IDMS_GDPRImplementation Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMS_Language_Matrix__mdt-IDMS Language Matrix Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMS_Locale_Matrix__mdt-IDMS Locale Matrix Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMS_SML__c-IDMS SML Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMS_Send_Invitation__c-IDMS Send Invitation Layout</layout></layoutAssignments>
<layoutAssignments><layout>IDMS_Social_Access__c-IDMS Social Access Layout</layout></layoutAssignments>
<layoutAssignments><layout>IPOLightRITE__c-IPO Light RITE Layout</layout></layoutAssignments>
<layoutAssignments><layout>ITB_Activity__c-ITB Activity Layout</layout></layoutAssignments>
<layoutAssignments><layout>ITB_Asset__c-AST - ITB Asset Layout</layout></layoutAssignments>
<layoutAssignments><layout>ITB_Entitlement__c-ENT - ITB Entitlement Layout</layout></layoutAssignments>
<layoutAssignments><layout>ITB_Warranty__c-WAR - ITB Warranty Layout</layout></layoutAssignments>
<layoutAssignments><layout>Idea-Idea Layout</layout></layoutAssignments>
<layoutAssignments><layout>Idms_Application_Hash__c-Idms Application Hash Layout</layout></layoutAssignments>
<layoutAssignments><layout>ImpactedInstalledProductFollowup__c-Impacted Installed Product Follow-up Layout</layout></layoutAssignments>
<layoutAssignments><layout>Impacted_Installed_Product__c-Impacted Installed Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>IndustryMembership__c-Industry Membership Layout</layout></layoutAssignments>
<layoutAssignments><layout>InfoQualifContact__c-Info France %28Contact%29 Layout</layout></layoutAssignments>
<layoutAssignments><layout>InfoQualif__c-Info France %28Account%29 Layout</layout></layoutAssignments>
<layoutAssignments><layout>InquiraFAQ__c-Inquira FAQ Layout</layout></layoutAssignments>
<layoutAssignments><layout>InstalledProductOnOpportunityNotif__c-Installed Product On Opportunity Notif Layout</layout></layoutAssignments>
<layoutAssignments><layout>InstalledProductOnOpportunity__c-Installed Product on Opportunity Layout</layout></layoutAssignments>
<layoutAssignments><layout>Installed_product_on_Opportunity_Line__c-Associated Installed Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>IntegrationCalloutEvent__c-IntegrationCalloutEvent Layout</layout></layoutAssignments>
<layoutAssignments><layout>IntegrationSimpleEvent__c-Integration Simple Event Layout</layout></layoutAssignments>
<layoutAssignments><layout>InterestAmountOnOpportunityNotif__c-Interest Amount Layout</layout></layoutAssignments>
<layoutAssignments><layout>InterestOnOpportunityNotif__c-Interest Layout</layout></layoutAssignments>
<layoutAssignments><layout>InvolvedOrganizationStakeholder__c-Involved Organization Stakeholder Layout</layout></layoutAssignments>
<layoutAssignments><layout>InvolvedOrganization__c-Involved Organization Layout</layout></layoutAssignments>
<layoutAssignments><layout>Issues_Risks__c-Issue</layout></layoutAssignments>
<layoutAssignments><layout>JctAssetLink__c-Asset Link Layout</layout></layoutAssignments>
<layoutAssignments><layout>Knowledge__kav-Knowledge Layout</layout></layoutAssignments>
<layoutAssignments><layout>LaunchReportStakeholders__c-ELLA Contacts Layout</layout></layoutAssignments>
<layoutAssignments><layout>Lead-LED - Lead Layout</layout></layoutAssignments>
<layoutAssignments><layout>LeadValueChainPlayer__c-Value Chain Player Layout</layout></layoutAssignments>
<layoutAssignments><layout>LegacyAccount__c-Legacy Account Layout</layout></layoutAssignments>
<layoutAssignments><layout>ListEmail-List Email Layout</layout></layoutAssignments>
<layoutAssignments><layout>LiteratureRequestLineItem__c-LiteratureRequest Line Item Layout</layout></layoutAssignments>
<layoutAssignments><layout>LiteratureRequest__c-LTR - Literature Request Layout</layout></layoutAssignments>
<layoutAssignments><layout>LiveAgentSession-Live Agent Session Layout</layout></layoutAssignments>
<layoutAssignments><layout>LiveAgent_ExtRoutingRule__c-External Chat Routing Rules Layout</layout></layoutAssignments>
<layoutAssignments><layout>LiveChatTranscript-Live Chat Transcript Layout</layout></layoutAssignments>
<layoutAssignments><layout>LiveChatTranscriptEvent-Live Chat Transcript Event Layout</layout></layoutAssignments>
<layoutAssignments><layout>LiveChatTranscriptWaiting-Live Chat Transcript %28Waiting%29 Layout</layout></layoutAssignments>
<layoutAssignments><layout>LiveChatVisitor-Live Chat Visitor Layout</layout></layoutAssignments>
<layoutAssignments><layout>LocalAttributeCountry__c-Local Attribute Country Layout</layout></layoutAssignments>
<layoutAssignments><layout>LocalAttribute__c-Local Attribute Layout</layout></layoutAssignments>
<layoutAssignments><layout>LocalizedFSB__c-Localized FSB Layout</layout></layoutAssignments>
<layoutAssignments><layout>Macro-Macro Layout</layout></layoutAssignments>
<layoutAssignments><layout>MarketSegmentCatalog__c-Market Segment Catalog Layout</layout></layoutAssignments>
<layoutAssignments><layout>MarketingCallbackLimit__c-MCL - Marketing Callback Limit Layout</layout></layoutAssignments>
<layoutAssignments><layout>MemberExternalProperties__c-MemberExternalProperties Layout</layout></layoutAssignments>
<layoutAssignments><layout>MembershipCatalog__c-MC - Membership Catalog Layout</layout></layoutAssignments>
<layoutAssignments><layout>MergeRequest__c-Merge Request Layout</layout></layoutAssignments>
<layoutAssignments><layout>Merge_Installed_Product__c-Merge Installed Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>Milestone1_Expense__c-Project Expense Layout</layout></layoutAssignments>
<layoutAssignments><layout>Milestone1_Log__c-Project Log Layout</layout></layoutAssignments>
<layoutAssignments><layout>Milestone1_Milestone__c-Project Milestone Layout</layout></layoutAssignments>
<layoutAssignments><layout>Milestone1_Project__c-Offer Product Project Layout</layout></layoutAssignments>
<layoutAssignments><layout>Milestone1_Task__c-Project Task Layout</layout></layoutAssignments>
<layoutAssignments><layout>Milestone1_Time__c-Project Time Layout</layout></layoutAssignments>
<layoutAssignments><layout>Milestone__c-Milestone Layout</layout></layoutAssignments>
<layoutAssignments><layout>Module__c-Module Layout</layout></layoutAssignments>
<layoutAssignments><layout>Multi_Platforms__c-Multi-Platform Layout</layout></layoutAssignments>
<layoutAssignments><layout>MySEChampionMapping__c-MySE Admin Mapping Layout</layout></layoutAssignments>
<layoutAssignments><layout>MySEUserActivityTrack__c-mySE User Activity Track Layout</layout></layoutAssignments>
<layoutAssignments><layout>OFMFPF__c-OFM FPF Layout</layout></layoutAssignments>
<layoutAssignments><layout>OIC_Quadruplet__c-OIC Quadruplet Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPP_ContractLink__c-Contract Link Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPP_DeliveryDetails__c-Delivery Details Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPP_OpportunityCompetitor__c-OCR - Opportunity Competitor Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPP_OpportunityLineCompetitor__c-Opportunity Line Competitor Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPP_OrderLink__c-ORL - Order Link Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPP_ProductLine__c-PRL - Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPP_Product__c-PRD - Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPP_ProjectSpecificInformation__c-Master Project Specific Information Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPP_ProjectTeam__c-Master Project Team Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPP_Project__c-Master Project Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPP_QuoteLink__c-QLK - Quote Link Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPP_SalesContributor__c-SCO - Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPP_SellingCenter__c-SLC - Selling Center Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPP_SupportRequest__c-SRQ - Solution Center Support Request Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPP_ValueChainPlayers__c-VCP - Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPQ_AssessmentQuestion__c-ASQ - Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPQ_AssessmentSection__c-ASC - Layout</layout></layoutAssignments>
<layoutAssignments><layout>OPQ_OpportunityQuestion__c-OPQ - Opportunity Question Layout</layout></layoutAssignments>
<layoutAssignments><layout>ORFFieldsConfiguration__c-ORF Field Configuration Layout</layout></layoutAssignments>
<layoutAssignments><layout>ORFwithOpportunities__c-ORF with Opportunities Layout</layout></layoutAssignments>
<layoutAssignments><layout>OSACMeeting__c-OSAC Meeting Layout</layout></layoutAssignments>
<layoutAssignments><layout>OSAProblemAlerts__c-OSA Problem Alerts Layout</layout></layoutAssignments>
<layoutAssignments><layout>ObjectSync__c-Object Sync Layout</layout></layoutAssignments>
<layoutAssignments><layout>ObjectivesDetail__c-OBD - Monthly Layout</layout></layoutAssignments>
<layoutAssignments><layout>ObjectivesSettings__c-OBS - Layout</layout></layoutAssignments>
<layoutAssignments><layout>OdasevaArchivingCaseHierarchy__c-OdasevaArchivingCaseHierarchy Layout</layout></layoutAssignments>
<layoutAssignments><layout>OdasevaArchivingLog__c-OdasevaArchivingLog Layout</layout></layoutAssignments>
<layoutAssignments><layout>OfferComponents__c-Offer Components Layout</layout></layoutAssignments>
<layoutAssignments><layout>OfferLifeCycleForecast__c-OfferLifeCycleForecast Layout</layout></layoutAssignments>
<layoutAssignments><layout>Offer_Lifecycle__c-Offer Launch - Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>Offer_Support__c-Business Dev Layout</layout></layoutAssignments>
<layoutAssignments><layout>Offers_Gouvernance__c-Offer Governance Layout Decision</layout></layoutAssignments>
<layoutAssignments><layout>OfmCountry__c-Country Layout</layout></layoutAssignments>
<layoutAssignments><layout>OppAgreement__c-AGR - Frame Agreement Layout</layout></layoutAssignments>
<layoutAssignments><layout>Opportunity-OPP_SOL - Stage 3 Layout</layout></layoutAssignments>
<layoutAssignments><layout>OpportunityAccountChallenge__c-Opportunity Account Challenge Layout</layout></layoutAssignments>
<layoutAssignments><layout>OpportunityAssessmentAnswer__c-Opportunity Assessment Answer Layout</layout></layoutAssignments>
<layoutAssignments><layout>OpportunityAssessmentQuestion__c-Opportunity Assessment Question Layout</layout></layoutAssignments>
<layoutAssignments><layout>OpportunityAssessmentResponse__c-Opportunity Assessment Response Layout</layout></layoutAssignments>
<layoutAssignments><layout>OpportunityAssessment__c-Opportunity Assessment Layout</layout></layoutAssignments>
<layoutAssignments><layout>OpportunityEcoStruxureLayer__mdt-Opportunity EcoStruxure Layer Layout</layout></layoutAssignments>
<layoutAssignments><layout>OpportunityEcoStruxureRule__mdt-Opportunity EcoStruxure Rule Layout</layout></layoutAssignments>
<layoutAssignments><layout>OpportunityLineItem-Opportunity Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>OpportunityNotification__c-Opportunity Notification Layout</layout></layoutAssignments>
<layoutAssignments><layout>OpportunityRegistrationForm__c-ORF Internal Layout</layout></layoutAssignments>
<layoutAssignments><layout>OpportunityRegistrationProducts__c-ORF Products - Internal Layout</layout></layoutAssignments>
<layoutAssignments><layout>OpportunitySplit-Opportunity Split Layout</layout></layoutAssignments>
<layoutAssignments><layout>OpportunityTeamMember-Opportunity Team Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>OpptyAmountThreshold__c-Oppty Amount Threshold Layout</layout></layoutAssignments>
<layoutAssignments><layout>OptyComplexityAssignmentRule__c-Opty Complexity Calculation Layout</layout></layoutAssignments>
<layoutAssignments><layout>OptyOwnerRoutingRule__c-OptyOwnerRoutingRule Layout</layout></layoutAssignments>
<layoutAssignments><layout>OrganizationStakeholdersForCR__c-Organization Stakeholders for CR Layout</layout></layoutAssignments>
<layoutAssignments><layout>PAMandCompetition__c-PAC - Layout</layout></layoutAssignments>
<layoutAssignments><layout>PAN_Notes_Database__c-PAN - Notes Database Layout</layout></layoutAssignments>
<layoutAssignments><layout>PLOperationHours__c-PLOperationHours Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRJLegalEntity__c-Legal Entity Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRJ_BudgetLine__c-Budget Line Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRJ_CostBreakdown__c-Project Cost breakdown Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRJ_Link__c-Link Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRJ_ProjectReq__c-Business Project Request Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRJ_Stakeholder__c-Project Actor Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMAccountBoxFolder__c-PRM Account Box Folder Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMAccountChannelChangeHistory__c-PRM Account Channel Change History Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMAccountOwnerAssignment__c-PRM Account Owner Assignment Rule Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMAttachments__c-PRM Attachments Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMBadgeBFOProperties__c-PRMBadgeBFOProperties Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMBoxFolder__c-PRM Box Folder Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMCertificationCatalog__c-Certification Catalog Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMChannelAccount__c-PRM Channel Account Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMChatComponentConfiguration__c-Chat Configuration Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMCockpitComponentConfiguration__c-PRMCockpitComponent Configuration Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMCockpitElementConfiguration__c-PRM CockpitElementConfiguration Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMConfigureEmailNotifications__c-PRM Configure Email Notification Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMCountryLanguageCodeMap__mdt-PRMCountryLanguageCodeMap Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMCountry__c-PRM Country Cluster Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMDistributorCatalog__c-PRM Distributor Catalog Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMDistributor__c-PRM Distributor Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMExternalPropertyEvent__c-External Propertiy Event Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMFrontEndLabels__c-PRM Partner Facing Front-end Labels Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMGroupAccProcessLog__c-PRM Group Account Process Log Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMLovsValues__c-PRM Lovs Value Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMLovs__c-PRM List of Values Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMMunchkinCodeConfig__mdt-PRM Munchkin Code Setting Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMPartnerMigration__c-PRM Partner Migration Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMPermissionSetTracking__c-PRM Permission Set Tracking Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMPrivacyLinks__mdt-PRM Privacy Link Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMProfileConfigInfo__c-PRM Profile Config Info Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMProfileConfig__c-Channel Progressive Profile Form</layout></layoutAssignments>
<layoutAssignments><layout>PRMPublishingSettings__mdt-PRM Publishing Setting Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMReactivationEmailHistory__c-PRM Reactivation Email History Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMRequestQueue__c-PRM Request Queue Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMRewardRedemptionShippedAddress__c-PRM Redemption Address Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMRewardRedemptionShippingAddress__c-Redemption Shipping Address Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMTechnicalMergeHistory__c-PRM Technical Merge History Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMTermsAndConditions__mdt-PRMOriginSource TnC Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMTnCHistory__c-Terms And Conditions History Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRMTransactionCatalog__c-Transaction Catalog Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRM_PAN_Request__c-PRM PAN Request Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRM_PCGroupNotificationFrequencyMap__mdt-PRM PC Group Notification Frequency Map Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRM_SST_Conf__c-BslScope Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRM_Technical_Login_History__c-PRM Technical Login History Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRM_Technical_Merge_Record__c-PRM Technical Merge Record Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRM_Technical_Partner_Record__c-PRM Technical Partner Record Layout</layout></layoutAssignments>
<layoutAssignments><layout>PRM_UploadFileExtensions__mdt-PRM Upload File Extension Layout</layout></layoutAssignments>
<layoutAssignments><layout>PackageTemplateCountry__c-PKC - General Layout</layout></layoutAssignments>
<layoutAssignments><layout>PackageTemplate__c-PKT - General Layout</layout></layoutAssignments>
<layoutAssignments><layout>Package__c-PKG %3A General Layout</layout></layoutAssignments>
<layoutAssignments><layout>PartnerAccessibleListViewConfig__c-Partner Accessible List View Config Layout</layout></layoutAssignments>
<layoutAssignments><layout>PartnerAccessibleListViewTranslation__c-Partner Accessible Listview Translation Layout</layout></layoutAssignments>
<layoutAssignments><layout>PartnerAssessment__c-Account Program Assessments%2FApplication Layout</layout></layoutAssignments>
<layoutAssignments><layout>PartnerFundAllocation-Partner Fund Allocation Layout</layout></layoutAssignments>
<layoutAssignments><layout>PartnerFundClaim-Partner Fund Claim Layout</layout></layoutAssignments>
<layoutAssignments><layout>PartnerFundRequest-Partner Fund Request Layout</layout></layoutAssignments>
<layoutAssignments><layout>PartnerLocator__c-Partner Locator Layout</layout></layoutAssignments>
<layoutAssignments><layout>PartnerMarketingBudget-Partner Marketing Budget Layout</layout></layoutAssignments>
<layoutAssignments><layout>PartnerOpportunityStatusHistory__c-Partner Opportunity Status History Layout</layout></layoutAssignments>
<layoutAssignments><layout>PartnerProgramClassification__c-Partner Program Classification Layout</layout></layoutAssignments>
<layoutAssignments><layout>PartnerProgramCountries__c-Partner Program Countries Layout</layout></layoutAssignments>
<layoutAssignments><layout>PartnerProgramMarket__c-Partner Program Market Layout</layout></layoutAssignments>
<layoutAssignments><layout>PartnerProgram__c-PRG - Country Partner Program Layout</layout></layoutAssignments>
<layoutAssignments><layout>PartnerRequest__c-Partner Request Layout</layout></layoutAssignments>
<layoutAssignments><layout>PerformanceTestRequest__c-Performance Test Request Layout</layout></layoutAssignments>
<layoutAssignments><layout>PermissionSetAssignmentRequests__c-Permission Set Assignment Request Layout</layout></layoutAssignments>
<layoutAssignments><layout>PermissionSetGroup__c-Permission Set Group Layout</layout></layoutAssignments>
<layoutAssignments><layout>PersonAccount-Person Account Layout</layout><recordType>PersonAccount.PersonAccount</recordType></layoutAssignments>
<layoutAssignments><layout>Plant__c-Plant Layout</layout></layoutAssignments>
<layoutAssignments><layout>PointOfContact__c-Point Of Contact Layout</layout></layoutAssignments>
<layoutAssignments><layout>PointsRegistrationHistory__c-Points Registration History Layout</layout></layoutAssignments>
<layoutAssignments><layout>Pre_study__c-Pre-study Layout</layout></layoutAssignments>
<layoutAssignments><layout>PreferredSupport__c-Preferred Support Layout</layout></layoutAssignments>
<layoutAssignments><layout>PreventiveAction__c-PRA- Schneider Preventive Action Layout</layout></layoutAssignments>
<layoutAssignments><layout>Pricebook2-Price Book Layout</layout></layoutAssignments>
<layoutAssignments><layout>PricebookEntry-Price Book Entry Layout</layout></layoutAssignments>
<layoutAssignments><layout>PrivateTestObject__c-PrivateTestObject Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProblemArchiveCaseLink__c-Problem - Archive Case Link Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProblemCaseLink__c-PCL - Problem To Case Link Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProblemCaseUnifiedLink__c-Problem-Case Unified Link Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProblemComplaintRequestLink__c-Problem-Complaint Request Link Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProblemStakeHolder__c-Problem Notifier Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProblemTeamMember__c-PTM - Problem Team Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>Problem__c-PRB - Schneider Problem Page Layout</layout></layoutAssignments>
<layoutAssignments><layout>Process_Stream__c-Process Stream Layout</layout></layoutAssignments>
<layoutAssignments><layout>Product2-Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProductLineCatalog__c-Product Line Catalog Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProductLineCompetitor__c-PLC - Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProductLineForAgreement__c-PLA - Agreement Product Line Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProductLineQualityContactMapping__c-Product Line Quality Contact Mapping Layout</layout></layoutAssignments>
<layoutAssignments><layout>Product_Quality_Alert__c-Product Quality Alert Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProfessionalMembership__c-Professional Membership Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProgramChangeRequest__c-PCR - Program Change Request Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProgramCostsForecasts__c-Program Costs Forecasts Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProgramDomainsOfExpertise__c-PDE - Program Domains Of Expertise Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProgramFeature__c-PRF - Country Feature Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProgramLevelBrand__c-Program Level Brand Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProgramLevel__c-PRL - Country Program Level Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProgramProduct__c-Program Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProgramRequirement__c-PRR - Activity Requirement Layout</layout></layoutAssignments>
<layoutAssignments><layout>Program__c-Program Update Layout</layout></layoutAssignments>
<layoutAssignments><layout>ProjectZoneCountry__c-Project Zone Countries Layout</layout></layoutAssignments>
<layoutAssignments><layout>Project_Event_link__c-Project - Event link Layout</layout></layoutAssignments>
<layoutAssignments><layout>Project_Snapshot__c-Project Snapshot Layout</layout></layoutAssignments>
<layoutAssignments><layout>Project__c-Project Layout</layout></layoutAssignments>
<layoutAssignments><layout>PsGroupMember__c-Permission Set Group Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>PublishPRMContent__c-Publish PRM Content Layout</layout></layoutAssignments>
<layoutAssignments><layout>QOTW__c-Quick Question Layout</layout></layoutAssignments>
<layoutAssignments><layout>QuestionTemplate__c-Question Template Layout</layout></layoutAssignments>
<layoutAssignments><layout>QuickText-Quick Text Layout</layout></layoutAssignments>
<layoutAssignments><layout>REF_OrganizationRecord__c-Organization Record Layout</layout></layoutAssignments>
<layoutAssignments><layout>REF_RoutingBOBS__c-Routing BO - BS Layout</layout></layoutAssignments>
<layoutAssignments><layout>REF_TeamRef__c-Team Reference Layout</layout></layoutAssignments>
<layoutAssignments><layout>RIT_BankGuarantee__c-BGT - Bank Guarantee Layout</layout></layoutAssignments>
<layoutAssignments><layout>RIT_BudgetLine__c-Budget Line Layout</layout></layoutAssignments>
<layoutAssignments><layout>RIT_Budget__c-BUD - Budget Layout</layout></layoutAssignments>
<layoutAssignments><layout>RIT_CommCompEnv__c-CCE - Commercial and competitive environments Layout</layout></layoutAssignments>
<layoutAssignments><layout>RIT_Consortium__c-CON - Consortium Layout</layout></layoutAssignments>
<layoutAssignments><layout>RIT_ContractualEnv__c-CTE - Contractual Environment Layout</layout></layoutAssignments>
<layoutAssignments><layout>RIT_FinancialEnv__c-FIN - Financial Environment Layout</layout></layoutAssignments>
<layoutAssignments><layout>RIT_LiquidatedDamages__c-LQD - Liquidated Damages Layout</layout></layoutAssignments>
<layoutAssignments><layout>RIT_ProjectInformation__c-PRI - Project Information Layout</layout></layoutAssignments>
<layoutAssignments><layout>RIT_ProjectManagement__c-PRM - Project Management Layout</layout></layoutAssignments>
<layoutAssignments><layout>RIT_Risks__c-RSK - Comm %26 Comp Layout</layout></layoutAssignments>
<layoutAssignments><layout>RIT_SMETeam__c-RITE - SME Team Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>RIT_SubContractOGSupp__c-SCO - Sub Contractors - OG Suppliers Layout</layout></layoutAssignments>
<layoutAssignments><layout>RIT_TechManScope__c-TMS - Technical - Manufacturing - Scope Layout</layout></layoutAssignments>
<layoutAssignments><layout>RIT_TermsAndMeansOfPayment__c-TMP - Terms And Means Of Payment Layout</layout></layoutAssignments>
<layoutAssignments><layout>RMAShippingToAdmin__c-RMA Shipping To Admin Layout</layout></layoutAssignments>
<layoutAssignments><layout>RMA_Product__c-Return Item Layout</layout></layoutAssignments>
<layoutAssignments><layout>RMA__c-Return Management Authorization Admin Layout</layout></layoutAssignments>
<layoutAssignments><layout>RPDR_Country__c-RPDR Country Layout</layout></layoutAssignments>
<layoutAssignments><layout>RPDR_Product__c-RPDT Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>RangesPerDeviceAndBrand__c-Ranges Per Device and Brand Layout</layout></layoutAssignments>
<layoutAssignments><layout>ReadOnlyTestObject__c-ReadOnlyTestObject Layout</layout></layoutAssignments>
<layoutAssignments><layout>Record__c-Record Layout</layout></layoutAssignments>
<layoutAssignments><layout>ReferentialDataError__c-Referential Data Errors Layout</layout></layoutAssignments>
<layoutAssignments><layout>ReferentialDataMapping__c-Referential Data Mapping Layout</layout></layoutAssignments>
<layoutAssignments><layout>RelatedOrganizationtoNotifyofXA__c-Related Organization to Notify of XA Layout</layout></layoutAssignments>
<layoutAssignments><layout>RelatedSymptom__c-Complaint Request Related Symptom Layout</layout></layoutAssignments>
<layoutAssignments><layout>Relationship_Suite__c-Relationship Suite Layout</layout></layoutAssignments>
<layoutAssignments><layout>Release__c-Master Release Layout</layout></layoutAssignments>
<layoutAssignments><layout>ReportStakeholder__c-Report Stakeholder Layout</layout></layoutAssignments>
<layoutAssignments><layout>ReportingSDH__c-ReportingSDH Layout</layout></layoutAssignments>
<layoutAssignments><layout>Request__c-Change Request</layout></layoutAssignments>
<layoutAssignments><layout>Request_for_Analysis__c-One-shot RFA</layout></layoutAssignments>
<layoutAssignments><layout>Request_work_in_progress__c-Request %28work in progress%29</layout></layoutAssignments>
<layoutAssignments><layout>Requested_Product__c-Requested Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>RequiredServiceSparePart__c-Required Service Spare Parts Layout</layout></layoutAssignments>
<layoutAssignments><layout>RequirementCatalog__c-REQ - Activity Requirement Layout</layout></layoutAssignments>
<layoutAssignments><layout>RequirementCertification__c-Requirement Certification Layout</layout></layoutAssignments>
<layoutAssignments><layout>RequirementSpecialization__c-Requirement Specialization Layout</layout></layoutAssignments>
<layoutAssignments><layout>ResolutionOptionAndCapabilities__c-Resolution Options and Capabilities Layout</layout></layoutAssignments>
<layoutAssignments><layout>ReturnItemsApprovalMatrix__c-Return Items Approval Matrix Layout</layout></layoutAssignments>
<layoutAssignments><layout>Revenue_Line__c-ITB Revenue Line Layout</layout></layoutAssignments>
<layoutAssignments><layout>Role__c-Account Layout</layout></layoutAssignments>
<layoutAssignments><layout>RootCause__c-RTC- Schneider RootCause Layout</layout></layoutAssignments>
<layoutAssignments><layout>SECCustomerExtension__c-SEC Customer Extension Layout</layout></layoutAssignments>
<layoutAssignments><layout>SECStaffExtension__c-SEC Staff Extension Layout</layout></layoutAssignments>
<layoutAssignments><layout>SEProblemSupplierProblemLink__c-SE Problem - Supplier Problem Link Layout</layout></layoutAssignments>
<layoutAssignments><layout>SE_Territory__c-TER- Layout</layout></layoutAssignments>
<layoutAssignments><layout>SFE_AccPlan__c-APL - Layout</layout></layoutAssignments>
<layoutAssignments><layout>SFE_CoachingVisitForm__c-CVF - Layout</layout></layoutAssignments>
<layoutAssignments><layout>SFE_EstimatedPlannedSales__c-EPS - Layout</layout></layoutAssignments>
<layoutAssignments><layout>SFE_IndivCAP__c-CAP - Layout</layout></layoutAssignments>
<layoutAssignments><layout>SFE_PlatformingScoring__c-SCO - Layout</layout></layoutAssignments>
<layoutAssignments><layout>SFE_Potential__c-APP - Layout</layout></layoutAssignments>
<layoutAssignments><layout>SOSSession-SOS Session Layout</layout></layoutAssignments>
<layoutAssignments><layout>SOSSessionActivity-SOS Session Activity Layout</layout></layoutAssignments>
<layoutAssignments><layout>SPAPricingDeskMember__c-SPA Pricing Desk Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>SPAQuotaQuarter__c-SPA Quota Quarter Layout</layout></layoutAssignments>
<layoutAssignments><layout>SPAQuota__c-SPA Quota Layout</layout></layoutAssignments>
<layoutAssignments><layout>SPARequestLineItem__c-SPA Request Line Item - Commercial Reference Layout</layout></layoutAssignments>
<layoutAssignments><layout>SPARequestPricingDeskMember__c-SPA Request Pricing Desk Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>SPARequest__c-SPA Request Layout</layout></layoutAssignments>
<layoutAssignments><layout>SPASalesGroup__c-SPA Sales Group Layout</layout></layoutAssignments>
<layoutAssignments><layout>SPAThresholdAmount__c-SPA Threshold Amount Layout</layout></layoutAssignments>
<layoutAssignments><layout>SPAThresholdDiscount__c-SPA Threshold Layout</layout></layoutAssignments>
<layoutAssignments><layout>SVMXC_ApplicableProduct__c-Applicable Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>SVMXC_Time_Entry_Request__c-Time Entry Request Layout</layout></layoutAssignments>
<layoutAssignments><layout>SVMXC_Time_Entry__c-Time Entry Layout</layout></layoutAssignments>
<layoutAssignments><layout>SVMXC_Timesheet__c-Timesheet Layout</layout></layoutAssignments>
<layoutAssignments><layout>SafeDomains__c-Safe Domains Layout</layout></layoutAssignments>
<layoutAssignments><layout>SalesEntity__c-SET - Layout</layout></layoutAssignments>
<layoutAssignments><layout>Scorecard-Scorecard Layout</layout></layoutAssignments>
<layoutAssignments><layout>ScorecardAssociation-Scorecard Association Layout</layout></layoutAssignments>
<layoutAssignments><layout>ScorecardMetric-Scorecard Metric Layout</layout></layoutAssignments>
<layoutAssignments><layout>SegmentSystemAlert__c-Segment System Alert Layout</layout></layoutAssignments>
<layoutAssignments><layout>ServiceContractValueChainPlayer__c-Service Contract Value Chain Player Layout</layout></layoutAssignments>
<layoutAssignments><layout>ServiceItem__c-Service Item Layout</layout></layoutAssignments>
<layoutAssignments><layout>ServiceLevelAgreement__c-SLA - Service Level Agreement Layout</layout></layoutAssignments>
<layoutAssignments><layout>SetupAlerts__c-PRM Country Cluster Layout</layout></layoutAssignments>
<layoutAssignments><layout>ShipHoldRequest__c-Ship Hold Request Layout</layout></layoutAssignments>
<layoutAssignments><layout>Solution-Solution Layout</layout></layoutAssignments>
<layoutAssignments><layout>SolutionApproach__c-Solution Approach Layout</layout></layoutAssignments>
<layoutAssignments><layout>SolutionCenterCountry__c-Solution Center Country Layout</layout></layoutAssignments>
<layoutAssignments><layout>SolutionCenter__c-SCR - Solution Center Layout</layout></layoutAssignments>
<layoutAssignments><layout>SpecializationCatalog__c-Specialization Catalog Layout</layout></layoutAssignments>
<layoutAssignments><layout>SpecializationClassification__c-Specialization Classification Layout</layout></layoutAssignments>
<layoutAssignments><layout>SpecializationCountry__c-Specialization Country Layout</layout></layoutAssignments>
<layoutAssignments><layout>SpecializationDomainsOfExpertise__c-Specialization Domains Of Expertise Layout</layout></layoutAssignments>
<layoutAssignments><layout>SpecializationMarket__c-Specialization Market Layout</layout></layoutAssignments>
<layoutAssignments><layout>SpecializationRequirement__c-SPR - Activity Specialization Requirement Layout</layout></layoutAssignments>
<layoutAssignments><layout>Specialization__c-Specialization Layout</layout></layoutAssignments>
<layoutAssignments><layout>SpiceGroupData__c-SpiceGroupData Layout</layout></layoutAssignments>
<layoutAssignments><layout>SpiceObjectData__c-Spice Object Data Layout</layout></layoutAssignments>
<layoutAssignments><layout>Stakeholders__c-Stakeholders Layout</layout></layoutAssignments>
<layoutAssignments><layout>StateProvince__c-STA - Layout</layout></layoutAssignments>
<layoutAssignments><layout>StepAction__c-Lock Record</layout></layoutAssignments>
<layoutAssignments><layout>Step__c-Final Step Layout</layout></layoutAssignments>
<layoutAssignments><layout>SubObjective__c-SOJ - Account</layout></layoutAssignments>
<layoutAssignments><layout>Subscriber__c-Subscriber Layout</layout></layoutAssignments>
<layoutAssignments><layout>Substitution__c-Substitution Layout</layout></layoutAssignments>
<layoutAssignments><layout>SuccessScorecard__c-Program Update Layout</layout></layoutAssignments>
<layoutAssignments><layout>SupplySustainablityRole__c-Supply %26 Sustainablity Role Layout</layout></layoutAssignments>
<layoutAssignments><layout>SupplyingPlant__c-SPP - Layout</layout></layoutAssignments>
<layoutAssignments><layout>Swap_Asset__c-Swap Asset Layout</layout></layoutAssignments>
<layoutAssignments><layout>Symptom__c-STM - Symptom Layout</layout></layoutAssignments>
<layoutAssignments><layout>System__c-System Layout</layout></layoutAssignments>
<layoutAssignments><layout>TECH_CommRefAllLevel__c-TECH_CommRefAllLevel Layout</layout></layoutAssignments>
<layoutAssignments><layout>TECH_GMRMapping__c-TECH_GMRMapping Layout</layout></layoutAssignments>
<layoutAssignments><layout>TEXStakeholder__c-TEX Stakeholder Layout</layout></layoutAssignments>
<layoutAssignments><layout>TEXTeamMember__c-TEX Team Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>TEX__c-TEX Layout</layout></layoutAssignments>
<layoutAssignments><layout>TMT_Curriculum__c-CUR - Trainer Layout</layout></layoutAssignments>
<layoutAssignments><layout>TMT_Location__c-LOC - Layout</layout></layoutAssignments>
<layoutAssignments><layout>TMT_Registration__c-REG - Trainer Layout</layout></layoutAssignments>
<layoutAssignments><layout>TMT_Session__c-SES - Trainer Layout</layout></layoutAssignments>
<layoutAssignments><layout>TMT_Teaching__c-TEA - Layout</layout></layoutAssignments>
<layoutAssignments><layout>Target_Products__c-Quality Alert Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>TargetedCountriesHeadCount__c-Targeted Countries Head Count Layout</layout></layoutAssignments>
<layoutAssignments><layout>TargetedCountries__c-Targeted Country Layout</layout></layoutAssignments>
<layoutAssignments><layout>Task-TSK - Standard Layout</layout></layoutAssignments>
<layoutAssignments><layout>TeamAgentMapping__c-TME - Page Layout</layout></layoutAssignments>
<layoutAssignments><layout>TeamCaseJunction__c-Supported Case Classification Layout</layout></layoutAssignments>
<layoutAssignments><layout>TeamMember__c-Program Team Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>TeamProductJunction__c-TPA - Page Layout</layout></layoutAssignments>
<layoutAssignments><layout>Team_Member__c-Team Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>Team_Members_Opportunity_Notification__c-Team Member Layout</layout></layoutAssignments>
<layoutAssignments><layout>Technical_Return__c-Technical Return Layout</layout></layoutAssignments>
<layoutAssignments><layout>Time_Sheet_Daily_Totals__c-Time Sheet Daily Totals Layout</layout></layoutAssignments>
<layoutAssignments><layout>TrackSetupAlets__c-Track Setup Alerts Layout</layout></layoutAssignments>
<layoutAssignments><layout>UAT_Results__c-UAT Results Layout</layout></layoutAssignments>
<layoutAssignments><layout>UIMSCompany__c-UIMS Company Layout</layout></layoutAssignments>
<layoutAssignments><layout>UIMS_Company__c-UIMS Company Layout</layout></layoutAssignments>
<layoutAssignments><layout>UIMS_User__c-UIMS User Layout</layout></layoutAssignments>
<layoutAssignments><layout>User-User Layout</layout></layoutAssignments>
<layoutAssignments><layout>UserAccessibleListViews__c-User Accessible List View Layout</layout></layoutAssignments>
<layoutAssignments><layout>UserAlt-User Profile Layout</layout></layoutAssignments>
<layoutAssignments><layout>UserAppMenuItem-Layout de Aplicativo</layout></layoutAssignments>
<layoutAssignments><layout>UserParameter__c-User Parameter Layout</layout></layoutAssignments>
<layoutAssignments><layout>UserProvAccount-User Provisioning Account Layout</layout></layoutAssignments>
<layoutAssignments><layout>UserProvisioningLog-User Provisioning Log Layout</layout></layoutAssignments>
<layoutAssignments><layout>UserProvisioningRequest-User Provisioning Request Layout</layout></layoutAssignments>
<layoutAssignments><layout>UserSAMLAssertion__c-User SAML Assertion Layout</layout></layoutAssignments>
<layoutAssignments><layout>UserStories__c-User Story Layout</layout></layoutAssignments>
<layoutAssignments><layout>User_Acceptance_Test__c-User Acceptance Test Layout</layout></layoutAssignments>
<layoutAssignments><layout>User_Story_Scenarios__c-User Story Scenario Layout</layout></layoutAssignments>
<layoutAssignments><layout>VCPOpportunityLineLink__c-VCP - Opportunity Line link Layout</layout></layoutAssignments>
<layoutAssignments><layout>Validated_Learning__c-Validated Learning Layout</layout></layoutAssignments>
<layoutAssignments><layout>View__c-Open Framework App View Layout</layout></layoutAssignments>
<layoutAssignments><layout>WONInstalledProductLine__c-WON Installed Product Line Layout</layout></layoutAssignments>
<layoutAssignments><layout>WO_ValueChainPlayer__c-WOVCP-Layout</layout></layoutAssignments>
<layoutAssignments><layout>WithdrawalReference__c-Withdrawal Reference Layout</layout></layoutAssignments>
<layoutAssignments><layout>WorkDetailDeletion__c-Work Detail Deletion Layout</layout></layoutAssignments>
<layoutAssignments><layout>WorkOrderAssignmentRule__c-Assignment Rule Layout</layout></layoutAssignments>
<layoutAssignments><layout>WorkOrderGroup__c-Work Order Group Layout</layout></layoutAssignments>
<layoutAssignments><layout>WorkOrderNote__c-Work Order Note Layout</layout></layoutAssignments>
<layoutAssignments><layout>WorkOrderNotification__c-Work Order Notification Layout</layout></layoutAssignments>
<layoutAssignments><layout>Work_Order_Rescheduling_Detail__c-Work Order Rescheduling Detail Layout</layout></layoutAssignments>
<layoutAssignments><layout>Work_Order_Skills__c-Work Order Skill Layout</layout></layoutAssignments>
<layoutAssignments><layout>Work_Order_Task__c-Work Order Task Layout</layout></layoutAssignments>
<layoutAssignments><layout>X3rd_Party_Cost__c-3rd Party Cost Layout</layout></layoutAssignments>
<layoutAssignments><layout>XAAffectedProduct__c-XA Affected Product Layout</layout></layoutAssignments>
<layoutAssignments><layout>ZHMIGateManagerList__c-Gate Manager Region Layout</layout></layoutAssignments>
<layoutAssignments><layout>ZProfile__c-Digital Services Profile Layout</layout></layoutAssignments>
<layoutAssignments><layout>bFO_Object__c-bFO Object Layout</layout></layoutAssignments>
<layoutAssignments><layout>bFoMatchingResult__c-bFO Matching Result Log Layout</layout></layoutAssignments>
<layoutAssignments><layout>groupappuserjunction__c-Group Layout</layout></layoutAssignments>
<layoutAssignments><layout>myFS_Message_Template__c-myFS Message Template Layout</layout></layoutAssignments>
<layoutAssignments><layout>myFS_Notification__c-myFS Notification Layout</layout></layoutAssignments>
<layoutAssignments><layout>odnoklassniki__mdt-odnoklassniki Layout</layout></layoutAssignments>
<layoutAssignments><layout>ofwgroup__c-Group Layout</layout></layoutAssignments>
<layoutAssignments><layout>transactionhistory__c-Transaction History Layout</layout></layoutAssignments>
<layoutAssignments><layout>vkontakte__mdt-session Layout</layout></layoutAssignments>
<objectPermissions><allowCreate>true</allowCreate><allowDelete>false</allowDelete><allowEdit>false</allowEdit><allowRead>true</allowRead><modifyAllRecords>false</modifyAllRecords><object>Account</object><viewAllRecords>false</viewAllRecords></objectPermissions>
<objectPermissions><allowCreate>true</allowCreate><allowDelete>false</allowDelete><allowEdit>false</allowEdit><allowRead>true</allowRead><modifyAllRecords>false</modifyAllRecords><object>Asset</object><viewAllRecords>false</viewAllRecords></objectPermissions>
<objectPermissions><allowCreate>true</allowCreate><allowDelete>false</allowDelete><allowEdit>false</allowEdit><allowRead>true</allowRead><modifyAllRecords>false</modifyAllRecords><object>Campaign</object><viewAllRecords>false</viewAllRecords></objectPermissions>
<objectPermissions><allowCreate>true</allowCreate><allowDelete>false</allowDelete><allowEdit>false</allowEdit><allowRead>true</allowRead><modifyAllRecords>false</modifyAllRecords><object>Case</object><viewAllRecords>false</viewAllRecords></objectPermissions>
<objectPermissions><allowCreate>true</allowCreate><allowDelete>false</allowDelete><allowEdit>false</allowEdit><allowRead>true</allowRead><modifyAllRecords>false</modifyAllRecords><object>Contact</object><viewAllRecords>false</viewAllRecords></objectPermissions>
<objectPermissions><allowCreate>true</allowCreate><allowDelete>true</allowDelete><allowEdit>true</allowEdit><allowRead>true</allowRead><modifyAllRecords>false</modifyAllRecords><object>Contract</object><viewAllRecords>false</viewAllRecords></objectPermissions>
<objectPermissions><allowCreate>false</allowCreate><allowDelete>false</allowDelete><allowEdit>false</allowEdit><allowRead>true</allowRead><modifyAllRecords>false</modifyAllRecords><object>Idea</object><viewAllRecords>false</viewAllRecords></objectPermissions>
<objectPermissions><allowCreate>true</allowCreate><allowDelete>false</allowDelete><allowEdit>false</allowEdit><allowRead>true</allowRead><modifyAllRecords>false</modifyAllRecords><object>Lead</object><viewAllRecords>false</viewAllRecords></objectPermissions>
<objectPermissions><allowCreate>true</allowCreate><allowDelete>false</allowDelete><allowEdit>false</allowEdit><allowRead>true</allowRead><modifyAllRecords>false</modifyAllRecords><object>Opportunity</object><viewAllRecords>false</viewAllRecords></objectPermissions>
<objectPermissions><allowCreate>false</allowCreate><allowDelete>false</allowDelete><allowEdit>false</allowEdit><allowRead>true</allowRead><modifyAllRecords>false</modifyAllRecords><object>Pricebook2</object><viewAllRecords>false</viewAllRecords></objectPermissions>
<objectPermissions><allowCreate>false</allowCreate><allowDelete>false</allowDelete><allowEdit>false</allowEdit><allowRead>true</allowRead><modifyAllRecords>false</modifyAllRecords><object>Product2</object><viewAllRecords>false</viewAllRecords></objectPermissions>
<objectPermissions><allowCreate>true</allowCreate><allowDelete>false</allowDelete><allowEdit>false</allowEdit><allowRead>true</allowRead><modifyAllRecords>false</modifyAllRecords><object>Solution</object><viewAllRecords>false</viewAllRecords></objectPermissions>
<pageAccesses><apexPage>BandwidthExceeded</apexPage><enabled>true</enabled></pageAccesses>
<pageAccesses><apexPage>Exception</apexPage><enabled>true</enabled></pageAccesses>
<pageAccesses><apexPage>FileNotFound</apexPage><enabled>true</enabled></pageAccesses>
<pageAccesses><apexPage>ForgotPassword</apexPage><enabled>true</enabled></pageAccesses>
<pageAccesses><apexPage>ForgotPasswordConfirm</apexPage><enabled>true</enabled></pageAccesses>
<pageAccesses><apexPage>InMaintenance</apexPage><enabled>true</enabled></pageAccesses>
<pageAccesses><apexPage>SiteLogin</apexPage><enabled>true</enabled></pageAccesses>
<pageAccesses><apexPage>SiteRegister</apexPage><enabled>true</enabled></pageAccesses>
<pageAccesses><apexPage>SiteRegisterConfirm</apexPage><enabled>true</enabled></pageAccesses>
<pageAccesses><apexPage>SiteTemplate</apexPage><enabled>true</enabled></pageAccesses>
<pageAccesses><apexPage>Unauthorized</apexPage><enabled>true</enabled></pageAccesses>
<pageAccesses><apexPage>VFP_CCCFollowupResponse</apexPage><enabled>true</enabled></pageAccesses>
<recordTypeVisibilities><default>true</default><recordType>Case.StandardCase</recordType><visible>true</visible></recordTypeVisibilities>
<recordTypeVisibilities><default>false</default><recordType>Feedback__c.Admin</recordType><visible>true</visible></recordTypeVisibilities>
<recordTypeVisibilities><default>true</default><recordType>Feedback__c.Idea</recordType><visible>true</visible></recordTypeVisibilities>
<recordTypeVisibilities><default>false</default><recordType>Feedback__c.Improvement</recordType><visible>true</visible></recordTypeVisibilities>
<recordTypeVisibilities><default>false</default><recordType>Feedback__c.Praise</recordType><visible>true</visible></recordTypeVisibilities>
<recordTypeVisibilities><default>true</default><recordType>Fielo_ListView__c.F_List</recordType><visible>true</visible></recordTypeVisibilities>
<recordTypeVisibilities><default>false</default><recordType>Fielo_ListView__c.Fielo_RelatedList</recordType><visible>true</visible></recordTypeVisibilities>
<recordTypeVisibilities><default>false</default><recordType>Fielo_PageComponent__c.Fielo_BlankSpace</recordType><visible>true</visible></recordTypeVisibilities>
<recordTypeVisibilities><default>true</default><recordType>Fielo_PageComponent__c.Fielo_CRUDButton</recordType><visible>true</visible></recordTypeVisibilities>
<recordTypeVisibilities><default>false</default><recordType>Fielo_PageComponent__c.Fielo_Field</recordType><visible>true</visible></recordTypeVisibilities>
<recordTypeVisibilities><default>false</default><recordType>Fielo_PageComponent__c.Fielo_HTMLContent</recordType><visible>true</visible></recordTypeVisibilities>
<recordTypeVisibilities><default>false</default><recordType>Fielo_PageComponent__c.Fielo_RelatedList</recordType><visible>true</visible></recordTypeVisibilities>
<tabVisibilities><tab>Knowledge__kav</tab><visibility>DefaultOn</visibility></tabVisibilities>
<tabVisibilities><tab>standard-ChannelProgram</tab><visibility>DefaultOff</visibility></tabVisibilities>
<tabVisibilities><tab>standard-ChannelProgramLevel</tab><visibility>DefaultOff</visibility></tabVisibilities>
<tabVisibilities><tab>standard-DuplicateRecordSet</tab><visibility>DefaultOn</visibility></tabVisibilities>
<tabVisibilities><tab>standard-Idea</tab><visibility>DefaultOn</visibility></tabVisibilities>
<tabVisibilities><tab>standard-PartnerFundAllocation</tab><visibility>DefaultOff</visibility></tabVisibilities>
<tabVisibilities><tab>standard-PartnerFundClaim</tab><visibility>DefaultOff</visibility></tabVisibilities>
<tabVisibilities><tab>standard-PartnerFundRequest</tab><visibility>DefaultOff</visibility></tabVisibilities>
<tabVisibilities><tab>standard-PartnerMarketingBudget</tab><visibility>DefaultOff</visibility></tabVisibilities>
<userLicense>Guest User License</userLicense>
<userPermissions><enabled>true</enabled><name>AllowUniversalSearch</name></userPermissions>
<userPermissions><enabled>true</enabled><name>ChatterEnabledForUser</name></userPermissions>
<userPermissions><enabled>true</enabled><name>EnableNotifications</name></userPermissions>
<userPermissions><enabled>true</enabled><name>PasswordNeverExpires</name></userPermissions>
<userPermissions><enabled>true</enabled><name>SelectFilesFromSalesforce</name></userPermissions>
<userPermissions><enabled>true</enabled><name>ShowCompanyNameAsUserBadge</name></userPermissions>
<userPermissions><enabled>true</enabled><name>UseWebLink</name></userPermissions>
<userPermissions><enabled>true</enabled><name>ViewAllUsers</name></userPermissions>


</Profile>
